/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IU;

import conexion.ConsultasDB;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Scanner;
import modelo.Partida;

/**
 *
 * @author Edwin
 */
public class Principal {

    public static Scanner t;
    public static Partida partida;
    public static ConsultasDB c;
    public static String[] pedirJugadores() {
        System.out.println("*****************Triqui 5x5*****************");
        String jugadores[] = {"", ""};
        System.out.print("Digite el nombre del jugador 1: ");
        jugadores[0] = t.next();
        System.out.print("Digite el nombre del jugador 2: ");
        jugadores[1] = t.next();
        return jugadores;
    }

    public static void mostrarTurno() {
        System.out.println("Turno " + (partida.getTurno() + 1) + ": " + partida.getJugadores()[partida.getTurno() % 2]);
    }

    public static void mostrarFichasSinUsar() {
        System.out.println(partida.buscarFichasSinUsar());
    }

    public static int pedirFicha() {
        try {
            System.out.print("Ingrese el número que corresponde a la ficha que desea usar: ");
            float ficha = t.nextInt();
            if (ficha % 1 != 0 || ficha < 0 || ficha > 36 || partida.comprobarFicha((int) ficha)) {
                throw new Exception();
            }
            return (int) ficha - 1;
        } catch (Exception e) {
            return pedirFicha();
        }
    }

    public static void mostrarTablero() {
        System.out.println(partida.getTablero().construirTablero());
    }

    public static int pedirPosicion() {
        try {
            System.out.print("Ingrese la posicion en la que desea poner la ficha: ");
            float pos = t.nextFloat();
            if (pos % 1 != 0 || pos < 0 || pos > 25) {
                throw new Exception();
            }
            return (int) pos - 1;
        } catch (Exception e) {
            return pedirPosicion();
        }
    }

    public static String insertarFicha(int f, int p) {
        return partida.usarFicha(f, p);
    }

    public static void avanzarTurno() {
        partida.avanzarTurno();
    }

    public static boolean comprobarOpcion() {
        try {
            System.out.print("Ingrese 1 para jugar , otro número para salir: ");
            int opc = t.nextInt();
            if (opc != 1) {
                System.out.println(partida.getJugadores()[partida.getTurno() % 2] + " Se ha rendido!. El ganador es: " + partida.getJugadores()[(partida.getTurno() + 1) % 2]);
            }
            return opc == 1;
        } catch (Exception e) {
            return comprobarOpcion();
        }
    }

    public static void mostrarGanador() {
        System.out.println("El ganador es: " + partida.getJugadores()[(partida.getTurno() + 1) % 2]);
    }

    public static void mostrarEmpate() {
        System.out.println("Se ha producido un empate.");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String url = "jdbc:sqlite:";
        String dir = "C:\\Users\\Juan Ayala\\TriquiDB.db";
        ConsultasDB c = new ConsultasDB(url, dir);
        t = new Scanner(System.in);
        String[] jugadores = pedirJugadores();
        partida = new Partida(jugadores);
        int ids[] = c.insertarJugadores(jugadores);
        int idPartida = c.insertarNuevaPartida(String.valueOf(LocalDate.now()), ids[0], ids[1]);
        int f, p=0;
        boolean r = true;
        while (!partida.hayTriqui(p) && !partida.tableroLleno() && r) {
            mostrarTurno();
            r = comprobarOpcion();
            if (r) {
                mostrarTablero();
                mostrarFichasSinUsar();
                p = pedirPosicion();
                f = pedirFicha();
                String fi  = insertarFicha(f, p);
                avanzarTurno();
                c.insertarNuevaJugada(idPartida, LocalDate.now()+""+LocalTime.now(),ids[partida.getTurno()%2], fi, p);
            }
        }
        mostrarTablero();
        if(partida.hayTriqui(p)){
            mostrarGanador();
        }
        if(partida.tableroLleno()){
            mostrarEmpate();
        }
    }
    

}
