/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IU;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.paint.Color;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.StageStyle;
import modelo.Cliente;
import modelo.Ficha;

/**
 *
 * @author Edwin
 */
//
///
///
public class GUI extends Application {

    private HBox panelSuperior;
    private HBox panelInferior;
    private VBox panelIzquierdo;
    private VBox panelDerecho;
    private GridPane panelCentral;
    private BorderPane distribucion;
    private Scene scene;
    private Button botonEntrarPartida;
    private Button botonRendirse;
    private Button botonSalir;
    private Button botonesTriqui[];
    //private Ficha[] fichas;
    private String jugador;
    private Cliente cliente;
    private int noJugador;
    private Thread hiloRefresco;
    private String[] actualizacion;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.inicializar(primaryStage);
    }

    public void inicializar(Stage primaryStage) {
        //this.inicFichas();
        //Configura la ventana
        Group root = new Group();
        scene = new Scene(root, 800, 600);
        primaryStage.setScene(scene);
        //Hace la ventana estatica en tamaño
        primaryStage.setResizable(false);

        //Modifica el titulo de la ventana
        primaryStage.setTitle("Juego Triqui");

        //Crea una distribucion de contenido
        this.distribucion = new BorderPane();
        this.distribucion.setTop(this.crearPanelSuperior());

        //Agrega la distribucion al root
        root.getChildren().add(this.distribucion);

        //Muestra la ventana
        primaryStage.show();

    }

    public HBox crearPanelInferior() {
        //Hbox: Panel en el cual el contenido se acomoda de forma horizontal
        this.panelInferior = new HBox();
        this.panelInferior.setStyle("-fx-background-color: cyan;");
        //Se crea el boton de rendicion
        this.botonRendirse = new Button("Rendirse");
        this.botonRendirse.setMinSize(100, 20);
        this.botonRendirse.setTranslateX(350);
        this.botonRendirse.setTranslateY(20);
        this.botonRendirse.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                botonSalir.setDisable(false);
                if (cliente.solicitarTurno() % 2 == (noJugador + 1) % 2) {
                    cliente.hacerJugada(jugador);
                    botonRendirse.setDisable(true);
                } else {
                    desplegarAlerta("Información.", "Esperando al otro jugador...");
                }

            }
        });
        //se crea el boton de salida
        this.botonSalir = new Button("Salir");
        this.botonSalir.setMinSize(100, 20);
        this.botonSalir.setTranslateX(500);
        this.botonSalir.setTranslateY(40);
        this.botonSalir.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.exit(0);
            }
        });
        this.botonSalir.setDisable(true);
        //Se agregan al panel
        this.panelInferior.getChildren().add(botonRendirse);
        this.panelInferior.getChildren().add(this.botonSalir);
        //Se modifica el tamaño del panel
        this.panelInferior.setMinSize(800, 100);
        return this.panelInferior;
    }

    public HBox crearPanelSuperior() {
        //Hbox: Panel en el cual el contenido se acomoda de forma horizontal
        this.panelSuperior = new HBox();
        this.panelSuperior.setStyle("-fx-background-color: cyan;");
        //Se crea el boton de entrar a una partida
        this.botonEntrarPartida = new Button("Entrar a partida");
        this.botonEntrarPartida.setMinSize(100, 20);
        this.botonEntrarPartida.setTranslateX(200);
        this.botonEntrarPartida.setTranslateY(20);
        this.botonEntrarPartida.setOnAction(this.botonEntrarAction(this.botonEntrarPartida));
        //Se agrega 
        this.panelSuperior.getChildren().add(this.botonEntrarPartida);
        //Se modifica el tamaño del panel
        this.panelSuperior.setMinSize(800, 600);

        return this.panelSuperior;
    }

    public VBox crearPanelIzquierdo() {
        this.panelIzquierdo = new VBox();
        this.panelIzquierdo.setStyle("-fx-background-color: cyan;");
        this.panelIzquierdo.setMinSize(200, 400);
        return this.panelIzquierdo;
    }

    public VBox crearPanelDerecho() {
        this.panelDerecho = new VBox();
        this.panelDerecho.setStyle("-fx-background-color: cyan;");
        this.panelDerecho.setMinSize(200, 400);
        return this.panelDerecho;
    }

    public GridPane crearPanelJuego() {
        this.botonesTriqui = new Button[25];
        this.panelCentral = new GridPane();
        this.panelCentral.setStyle("-fx-background-color: red;");
        int p = 0;
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                this.botonesTriqui[p] = new Button(String.valueOf(p));
                this.botonesTriqui[p].setMinSize(80, 80);
                this.botonesTriqui[p].setOnAction(this.botonesTriquiAccion(this.botonesTriqui[p]));
                this.panelCentral.add(this.botonesTriqui[p], i, j);
                p++;
            }
        }
        return panelCentral;
    }

    private void desplegarAlerta(String t, String msg) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle(t);
        alert.setHeaderText(null);
        alert.setContentText(msg);
        alert.showAndWait();
    }

    private String desplegarDialogoEntrada(String t, String msg) {
        TextInputDialog dialogoAlerta = new TextInputDialog();//Se crea el tipo de cuadro
        dialogoAlerta.setTitle(t);//Titulo de ventana 1
        dialogoAlerta.setHeaderText("hola");
        dialogoAlerta.setContentText(msg);// Leyenda ventana 1
        dialogoAlerta.initStyle(StageStyle.UTILITY);
        Optional<String> respuesta = dialogoAlerta.showAndWait();//Se obtiene la respuesta del jugador 1.
        if (respuesta.isPresent()) {
            String r = respuesta.get();
            System.out.println(r);
            return r;
        } else {
            return this.desplegarDialogoEntrada(t, msg);
        }
    }

    private EventHandler<ActionEvent> botonEntrarAction(Button b) {
        EventHandler<ActionEvent> evento = (ActionEvent) -> {

            this.jugador = this.desplegarDialogoEntrada("Ingreso de datos.", "Ingrese su nombre: ");
            this.panelSuperior.setMinSize(800, 100);
            this.distribucion.setBottom(this.crearPanelInferior());
            this.distribucion.setCenter(this.crearPanelJuego());
            this.distribucion.setRight(this.crearPanelDerecho());
            this.distribucion.setLeft(this.crearPanelIzquierdo());
            String ip = this.desplegarDialogoEntrada("Ingreso de datos.", "Ingrese la ip del servidor");
            System.out.println("ip " + ip);
            System.out.println("Jugador" + jugador);
            this.noJugador = cliente.iniciar(jugador) ? 2 : 1;

            this.hiloRefresco.start();
            b.setDisable(true);
        };
        return evento;
    }

    public void crearHilo() {
        this.hiloRefresco = new Thread() {
            String[] aux;

            @Override
            public void run() {
                while (true) {
                    if ((cliente.solicitarTurno() % 2 == (noJugador % 2))) {
                        aux = cliente.solicitarActualizacion();
                        if (!this.comparar(actualizacion, aux)) {
                            actualizacion = aux;
                        }
                    }
                }
            }

            public boolean comparar(String[] a1, String[] a2) {
                for (int i = 0; i < a1.length; i++) {
                    if (a1[i].compareTo(a2[i]) != 0) {
                        return false;
                    }
                }
                return true;
            }
        };
    }

    public void actualizar() {
        this.botonesTriqui[Integer.valueOf(this.actualizacion[2])].setText(this.actualizacion[1]);
        this.botonesTriqui[Integer.valueOf(this.actualizacion[2])].setStyle("-fx-text-fill: green");
    }

    public String mostrarFichasDisponibles(String fichasD[]) {
        ChoiceDialog<String> dialog = new ChoiceDialog<>(fichasD[0], fichasD);
        dialog.initStyle(StageStyle.UTILITY);
        dialog.setTitle("Entrada de datos.");
        dialog.setHeaderText("Fichas disponibles.");
        dialog.setContentText("Escoja una ficha: ");
        Optional<String> result = dialog.showAndWait();
        return result.get();
    }

    public EventHandler<ActionEvent> botonesTriquiAccion(Button b) {
        EventHandler<ActionEvent> evento = (ActionEvent) -> {
            //Hacer solicitud al servidor para ver entre cuales fichas puedo escoger
            //Fichas de muestra(Seran removidas en futuras versiones)
            if (cliente.solicitarTurno() % 2 == ((this.noJugador + 1) % 2)) {
                String[] fichasD = cliente.solicitarFichasDisponibles();
                String r = this.mostrarFichasDisponibles(fichasD);
                int pos = Integer.valueOf(b.getText());
                b.setText(r);
                this.cliente.hacerJugada(jugador, r, pos);
                b.setDisable(true);
            } else {
                this.desplegarAlerta("Información.", "Esperando al otro jugador...");
            }
        };
        return evento;
    }

    /*public final void inicFichas() {
        fichas = new Ficha[36];
        String formas[] = {"Circulo", "Triangulo", "Square"};
        String tamanos[] = {"Grande", "Pequeño"};
        String colores[] = {"Rojo", "Verde", "Negro"};
        boolean punto[] = {true, false};
        int i = 0;
        for (String forma : formas) {
            for (String tamano : tamanos) {
                for (String color : colores) {
                    fichas[i] = new Ficha(forma, tamano, color, true);
                    fichas[i + 1] = new Ficha(forma, tamano, color, false);
                    i += 2;
                }
            }
        }
    }*/
    public static void main(String[] args) {
        launch(args);
    }

}
