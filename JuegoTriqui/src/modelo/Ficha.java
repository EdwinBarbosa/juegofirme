/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Edwin
 */
public class Ficha{

    private String forma;
    private String tamano;
    private String color;
    private boolean punto;

    public Ficha(String forma, String tamano, String color, boolean punto) {
        this.forma = forma;
        this.tamano = tamano;
        this.color = color;
        this.punto = punto;
    }

    public boolean compararFichas(Ficha f) {
        return resumirContenido().compareToIgnoreCase(f.resumirContenido()) == 0;
    }
    
    public String resumirContenido(){
        String punt = isPunto() ? "C":"S";
        return String.valueOf(getForma().charAt(0))+String.valueOf(getTamano().charAt(0))
                + String.valueOf(getColor().charAt(0)) + punt;
    }

    public String getForma() {
        return forma;
    }

    public String getTamano() {
        return tamano;
    }

    public String getColor() {
        return color;
    }

    public boolean isPunto() {
        return punto;
    }
}
