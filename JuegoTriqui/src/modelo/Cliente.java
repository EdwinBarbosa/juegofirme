package modelo;

/*
 * Librería xmlrpc:
 * http://archive.apache.org/dist/ws/xmlrpc/binaries/apache-xmlrpc-3.1.3-bin.zip
 */

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

public class Cliente {

    private XmlRpcClient client = new XmlRpcClient();
    private String URL;

    public Cliente(String ip) {
        this.URL = ip + ":8080/xmlrpc";
        // Crea la configuración del cliente
        XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
        try {
            config.setServerURL(new URL(this.URL));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        // Crea el cliente y asigna la configuración
        XmlRpcClient client = new XmlRpcClient();
        client.setConfig(config);
    }
    
    public boolean iniciar(String jugador) {
        Object[] params = new Object[]{jugador};
        try {
            // Ejecuta el método add en la clase Calculator con sus respectivos parámetros
            boolean result = (boolean) client.execute("Servicios.iniciar", params);
            // Imprime el resultado
            System.out.println("El resultado de iniciar fue " + result);
            return result;
        } catch (XmlRpcException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public boolean confirmarInicio() {
        Object[] params = new Object[]{};
        try {
            // Ejecuta el método add en la clase Calculator con sus respectivos parámetros
            boolean result = (boolean) client.execute("Servicios.confirmarInicio", params);
            // Imprime el resultado
            System.out.println("El resultado fue " + result);
            return result;
        } catch (XmlRpcException e) {
            e.printStackTrace();
        }
        return false;
    }
    public boolean hacerJugada(String j){
        Object[] params = new Object[]{j};
        try {
            // Ejecuta el método add en la clase Calculator con sus respectivos parámetros
            boolean result = (boolean) client.execute("Servicios.hacerJugada", params);
            // Imprime el resultado
            System.out.println("El resultado fue " + result);
            return result;
        } catch (XmlRpcException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public boolean hacerJugada(String j ,String ficha, int posicion) {
        Object[] params = new Object[]{j,ficha,posicion};
        try {
            // Ejecuta el método add en la clase Calculator con sus respectivos parámetros
            boolean result = (boolean) client.execute("Servicios.hacerJugada", params);
            // Imprime el resultado
            System.out.println("El resultado fue " + result);
            return result;
        } catch (XmlRpcException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public String[] solicitarActualizacion(){
        Object[] params = new Object[]{};
        try {
            // Ejecuta el método add en la clase Calculator con sus respectivos parámetros
            String[] result = (String[]) client.execute("Servicios.solicitarActualizacion", params);
            // Imprime el resultado
            //System.out.println("El resultado fue " + result);
            return result;
        } catch (XmlRpcException e) {
            e.printStackTrace();
        }
        return null;
    }
    public String[] solicitarFichasDisponibles(){
        Object[] params = new Object[]{};
        try {
            // Ejecuta el método add en la clase Calculator con sus respectivos parámetros
            String result = (String) client.execute("Servicios.solicitarFichasDisponibles", params);
            // Imprime el resultado
            //System.out.println("El resultado fue " + result);
            return result.split(";");
        } catch (XmlRpcException e) {
            e.printStackTrace();
        }
        return null;
    }
    public int solicitarTurno(){
        Object[] params = new Object[]{};
        try {
            // Ejecuta el método add en la clase Calculator con sus respectivos parámetros
            int result = (int) client.execute("Servicios.solicitarTurno", params);
            // Imprime el resultado
            //System.out.println("El resultado fue " + result);
            return result;
        } catch (XmlRpcException e) {
            e.printStackTrace();
        }
        return -1;
    }
    public int confirmarResultado() {
        Object[] params = new Object[]{};
        try {
            // Ejecuta el método add en la clase Calculator con sus respectivos parámetros
            int result = (int) client.execute("Servicios.confirmarResultado", params);
            // Imprime el resultado
            //System.out.println("El resultado fue " + result);
            return result;
        } catch (XmlRpcException e) {
            e.printStackTrace();
        }
        return -1;
    }
    

}
