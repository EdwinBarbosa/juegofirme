/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Edwin
 */
public class Tablero {

    private Ficha casillas[][];

    public Tablero() {
        casillas = new Ficha[5][5];
    }

    public void agregarFicha(Ficha f, int[] posicion) {
        casillas[posicion[0]][posicion[1]] = f;
    }

    public boolean comprobarTriqui(int[] pos) {
        Ficha[] linea = new Ficha[5];
        boolean b;
        for (int i = 0; i < 5; i++) {
            linea[i] = casillas[pos[0]][i];
        }
        b = this.comprobarLinea(linea);
        if (b) {
            return b;
        }
        for (int i = 0; i < 5; i++) {
            linea[i] = casillas[i][pos[1]];
        }
        b = this.comprobarLinea(linea);
        if (b) {
            return b;
        }
        for (int i = 0; i < 5; i++) {
            linea[i] = casillas[i][i];
        }
        b = this.comprobarLinea(linea);
        if (b) {
            return b;
        }
        for (int i = 0; i < 5; i++) {
            linea[i] = casillas[4 - i][i];
        }
        b = this.comprobarLinea(linea);
        if (b) {
            return b;
        }
        return false;
    }

    public boolean comprobarTableroLleno() {
        for (Ficha[] linea : casillas) {
            for (Ficha f : linea) {
                if (f == null) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean comprobarLinea(Ficha[] linea) {
        for (Ficha f : linea) {
            if (f == null) {
                return false;
            }
        }
        boolean comun;
        for (int i = 0; i < 4; i++) {
            comun = true;
            for (Ficha f : linea) {
                if (f.resumirContenido().charAt(i) != linea[0].resumirContenido().charAt(i)) {
                    comun = false;
                    break;
                }
            }
            if (comun) {
                return true;
            }
        }
        return false;
    }

    public String construirTablero() {
        String tablero = "";
        int c = 1;
        for (int i = 0; i < 5; i++) {
            if (i > 0) {
                tablero = tablero + "\n----------------------------------\n";
            }
            for (int j = 0; j < 5; j++) {
                if (j > 0) {
                    tablero = tablero + "|";
                }
                if (casillas[i][j] == null) {
                    if (c < 10) {
                        tablero = tablero + "  0";
                    } else {
                        tablero = tablero + "  ";
                    }
                    tablero = tablero + String.valueOf(c) + "  ";

                } else {
                    tablero = tablero + " " + casillas[i][j].resumirContenido() + " ";
                }
                c++;
            }
        }
        return tablero;
    }
}
