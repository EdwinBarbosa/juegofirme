/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import static IU.Principal.partida;
import conexion.ConsultasDB;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

/**
 *
 * @author Edwin
 */
public class Servicios {

    private final String url = "jdbc:sqlite:";
    private final String dir = "C:\\Users\\Juan Ayala\\TriquiDB.db";
    private ConsultasDB consultor = new ConsultasDB(url, dir);;
    private ArrayList<String> jugadores  = new ArrayList<>();
    private Partida p;
    private int ids[];
    private int idPartida;
    private String rendicion = "";
    private String[] ultimaJugada = new String[]{"", "", "0"};


    public boolean iniciar(String jugador) {
        System.out.println("Iniciar...");
        this.jugadores.add(jugador);
        if (this.confirmarInicio()) {
            String[] j = (String[]) jugadores.toArray();
            this.p = new Partida(j);
            this.ids = this.consultor.insertarJugadores(j);
            this.idPartida = consultor.insertarNuevaPartida(String.valueOf(LocalDate.now()), ids[0], ids[1]);
        }
        return this.confirmarInicio();
    }

    public boolean confirmarInicio() {
        System.out.println("size " + this.jugadores.size());
        return this.jugadores.size() == 2;
    }

    public boolean hacerJugada(String j) {
        this.rendicion = j;
        return true;
    }

    public boolean hacerJugada(String j, String ficha, int posicion) {
        String fi = this.p.usarFicha(this.p.buscarFicha(ficha), posicion);
        this.consultor.insertarNuevaJugada(idPartida, LocalDate.now() + "" + LocalTime.now(), ids[partida.getTurno() % 2], fi, posicion);
        this.ultimaJugada = new String[]{j, fi, String.valueOf(posicion)};
        this.p.avanzarTurno();
        return fi.compareTo("") != 0;
    }

    public Object[] solicitarActualizacion() {
        return this.ultimaJugada;
    }

    public String solicitarFichasDisponibles() {
        return this.p.buscarFichasSinUsar();
    }

    public int solicitarTurno() {
        return this.p.getTurno();
    }

    public int confirmarResultado() {
        return this.p.hayTriqui(this.p.buscarFicha(this.ultimaJugada[2])) ? 1 : this.p.tableroLleno() ? 2 : this.rendicion.compareTo("") != 0 ? 3 : 0;
    }
}
