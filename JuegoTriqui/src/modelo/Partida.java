/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Edwin
 */
public class Partida {
    private Ficha[] fichas;
    private String[] jugadores;
    private Tablero tablero;
    private int turno;
    
    public Partida(String[] jugadores){
        this.inicFichas();
        this.jugadores = jugadores;
        this.tablero = new Tablero();
        turno = 0;
    }
    public boolean hayTriqui(int p){
        if(p!=0)
            return this.tablero.comprobarTriqui(this.convertirPos(p));
        else
            return false;
    }
    public boolean tableroLleno(){
        return this.tablero.comprobarTableroLleno();
    }
    public Tablero getTablero() {
        return tablero;
    }

    public String[] getJugadores() {
        return jugadores;
    }

    public int getTurno() {
        return turno;
    }
    
    public void avanzarTurno(){
        turno++;
    }
    
    public String buscarFichasSinUsar(){
        String s = "";
        for(Ficha f : fichas){
            if(f != null){
                s = s + f.resumirContenido()+";";
            }
        }
        return s;
    }
    
    public boolean comprobarFicha(int n){
        return fichas[n] == null;
    }
    
    public String usarFicha(int fi,int p){
        String s = "";
        if(fichas[fi]!= null){
            this.tablero.agregarFicha(fichas[fi], this.convertirPos(p));
            s = fichas[fi].resumirContenido();
            fichas[fi] = null;
        }
        return s;
    }
    public int buscarFicha(String f){
        for (int i = 0; i < fichas.length; i++) {
            if(f.compareTo(this.fichas[i].resumirContenido())==0){
                return i;
            }
        }
        return -1;
    }
    public int[] convertirPos(int p){
        int c = 0;
        for(int i = 0;i < 5 ; i++){
            for(int j = 0;j < 5 ; j++){
                if(c == p){
                    return new int[]{i,j};
                }
                c++;
            }
        }
        return null;
    }
    
    
    public final void inicFichas(){
        fichas = new Ficha[36];
        String formas[] = {"Circulo", "Triangulo", "Square"};
        String tamanos[] = {"Grande", "Pequeño"};
        String colores[] = {"Rojo", "Verde", "Negro"};
        boolean punto[] = {true, false};
        int i = 0;
        for (String forma : formas) {
            for (String tamano : tamanos) {
                for (String color : colores) {
                        fichas[i] = new Ficha(forma, tamano, color, true);
                        fichas[i+1] = new Ficha(forma, tamano, color, false);
                        i+=2;
                }
            }
        }
    }
    
}
