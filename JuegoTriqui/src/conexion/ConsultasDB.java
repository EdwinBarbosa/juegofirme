/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author edwin
 */
public class ConsultasDB {
    private ConexionDB DB;
    private Connection conexion;
    private String sql;
    private PreparedStatement ps;
    private ResultSet rs;
    public ConsultasDB(String url,String ruta) {
        DB = new ConexionDB(url,ruta);
    }

    public int insertarNuevaPartida(String fecha, int idJ1, int idJ2) {
        try {
            conexion = DB.obtenerConexion();
            sql = "INSERT INTO PARTIDAS(Fecha,Id_JugadorNo1,Id_JugadorNo2) values (?,?,?)";
            ps = conexion.prepareStatement(sql);
            ps.setString(1, fecha);
            ps.setInt(2, idJ1);
            ps.setInt(3, idJ2);
            ps.executeUpdate();
            Statement st = conexion.createStatement();
            sql = "SELECT max(Id_Partida) FROM PARTIDAS";
            rs   = st.executeQuery(sql);
            rs.next();
            int id = rs.getInt("max(Id_Partida)");
            DB.desconectarDB();
            return id;
        } catch (SQLException ex) {
            Logger.getLogger(ConsultasDB.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
        
    }
    public int insertarNuevaJugada(int idPartida,String fecha,int idJugador,String ficha,int Casilla){
        try {
            conexion = DB.obtenerConexion();
            sql = "INSERT INTO MOVIMIENTOS(Id_Partida1,Fecha,Id_Jugador1,Ficha,Casilla) values (?,?,?,?,?)";
            ps = conexion.prepareStatement(sql);
            ps.setInt(1, idPartida);
            ps.setString(2, fecha);
            ps.setInt(3, idJugador);
            ps.setString(4, ficha);
            ps.setInt(5, Casilla);
            ps.executeUpdate();
            Statement st = conexion.createStatement();
            sql = "SELECT max(Id_Movimiento) FROM MOVIMIENTOS";
            rs   = st.executeQuery(sql);
            rs.next();
            int id = rs.getInt("max(Id_Movimiento)");
            DB.desconectarDB();
            return id;
        } catch (SQLException ex) {
            Logger.getLogger(ConsultasDB.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }
    public int[] insertarJugadores(String[] jugadores){
        try {
            conexion = DB.obtenerConexion();
            sql = "INSERT INTO Jugadores(Nombre) values (?)";
            ps = conexion.prepareStatement(sql);
            ps.setString(1, jugadores[0]);
            ps.executeUpdate();
            ps.setString(1, jugadores[1]);
            Statement st = conexion.createStatement();
            sql = "SELECT max(Id_Jugador) FROM Jugadores";
            rs   = st.executeQuery(sql);
            rs.next();
            int id = rs.getInt("max(Id_Jugador)");
            DB.desconectarDB();
            return new int[]{id-1,id};
        } catch (SQLException ex) {
            Logger.getLogger(ConsultasDB.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
