/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;


import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 *
 * @author edwin
 */
public class ConexionDB {
    private String url;
    private String ruta;
    private Connection conexion;
    public ConexionDB(String url, String ruta) {
        this.url = url;
        this.ruta = ruta;
    }
    
    public Connection obtenerConexion() {
        try {
            conexion = DriverManager.getConnection(url+ruta);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conexion;
    }
    public void desconectarDB(){
        try {
            conexion.close();
        } catch (SQLException ex) {
            Logger.getLogger(ConexionDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
